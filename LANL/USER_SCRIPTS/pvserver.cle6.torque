#!/bin/bash

echo "---------- BEGIN PVSERVER LAUNCHER ----------"
date +"%F %T"
echo "---------------------------------------------"

source /opt/modules/default/init/bash 1>/dev/null 2>/dev/null
module load sandbox
module load friendly-testing
module load paraview/${PV_VERSION_FULL}-osmesa

# Cores Per Node
NUM_CPN=$(aprun rep -c "^processor" /proc/cpuinfo)

# Threads Per Process
NUM_TPP=$((NUM_CPN/NUM_PPN))
echo "Setting ${NUM_TPP} threads per process"

if [ "${DRIVER}" = "swr" ]
then
  export GALLIUM_DRIVER=swr
  export KNOB_MAX_WORKER_THREADS=${NUM_TPP}
else
  export GALLIUM_DRIVER=llvmpipe
  export LP_NUM_THREADS=${NUM_TPP}
fi

INT_IP=$(/sbin/ip addr show dev ${INT_IF} | sed -n 's|.*inet \([0-9\.]*\).*|\1|p')
echo "Starting socat tunnel ${INT_IP}:${PV_SERVER_PORT} -> ${FE_IP}:${PV_SERVER_PORT}"
export PATH=$(dirname ${BASH_SOURCE}):${PATH}
socat -d -d -lf socat-int.log \
  tcp4-listen:${PV_SERVER_PORT},bind=${INT_IP},close \
  tcp4-connect:${FE_IP}:${PV_SERVER_PORT} &
SOCAT_PID=$!

NUM_TASKS=$((NUM_NODES*NUM_PPN))
aprun -n ${NUM_TASKS} -d ${NUM_TPP} -N ${NUM_PPN} \
  pvserver \
    --use-offscreen-rendering \
    -rc \
    -ch=${INT_IP} \
    -sp=${PV_SERVER_PORT} \
    --timeout=$((HOURS*60))
