#!/bin/bash

echo "---------- BEGIN PVSERVER LAUNCHER ----------"
date +"%F %T"
echo "---------------------------------------------"

source /opt/modules/default/init/bash
module purge
module load sandbox
module load friendly-testing
module load paraview/${PV_VERSION_FULL}-osmesa

# Cores Per Node
NUM_CPN=$(grep -c "^processor" /proc/cpuinfo)

# Threads Per Process
NUM_TPP=$((NUM_CPN/NUM_PPN))
echo "Setting ${NUM_TPP} threads per process"

if [ "${DRIVER}" = "swr" ]
then
  export GALLIUM_DRIVER=swr
  export KNOB_MAX_WORKER_THREADS=${NUM_TPP}
else
  export GALLIUM_DRIVER=llvmpipe
  export LP_NUM_THREADS=${NUM_TPP}
fi
export OSPRAY_THREADS=${NUM_TPP}

NUM_TASKS=$((NUM_NODES*NUM_PPN))

# We need to use the numabind script here since slurm does a poor job at
# setting the appropriate affinity and ens up piling evrything onto one socket
/usr/bin/srun -n ${NUM_TASKS} -N ${NUM_NODES} -c${NUM_TPP} \
  --ntasks-per-node=${NUM_PPN} \
  ${NUMABIND} pvserver \
    --use-offscreen-rendering \
    -rc \
    -ch=${PV_SERVER_HOST} \
    -sp=${PV_SERVER_PORT} \
    --timeout=$((HOURS*60))
