################################################################################
# Helper functions for the main script
################################################################################

# Sleep for a given number of seconds, outputting ., *, or | depending on how
# many total seconds have elapsed
sleep_print() {
  if [ -z "${S}" ]
  then
    S=0
  fi

  sleep $1
  S=$((S+5))
  if [ $((S%3600)) -eq 0 ]
  then
    echo -n '|'
  elif [ $((S%60)) -eq 0 ]
  then
    echo -n '*'
  else
    echo -n '.'
  fi
}

# Export a list of variables in "VAR1=VALUE1 VAR2=VALUE2 ..." format
export_vars() {
  for VV in $@
  do
    EQIDX=$(expr index "${VV}" "=")
    if [ ${EQIDX} -eq 0 ]
    then
      echo "  Warning: Bad format: Skipping \"${VV}\""
      continue
    fi
    VARNAME=${VV:0:EQIDX-1}
    VARVALUE="${VV:${EQIDX}}"
    eval export ${VARNAME}=\"\${VARVALUE}\"
    echo "  Exported ${VARNAME} = \"${VARVALUE}\""
  done
}

# Kill the tunnel process if it exists
cleanup_tunnel() {
  if [ -n "${TUNNEL_PID}" ]
  then
    kill -9 ${TUNNEL_PID} 2>/dev/null
  fi
}

################################################################################
# Implementation functions for setup_tunnel
################################################################################

# Dummy tunnel function.  Use this when a port forwarding tunnel has already
# been establised (using GatewayPorts with ssh for example)
setup_tunnel_noop() {
  return 0
}

# Use ssh tunnel to to the rank 0 host
# Input arguments:
#   R0HOST - Hostname to establish the ssh connection to
#   PORT   - Port number to establish the reverse tunnel for
# Return code:
#   0 - Tunnel successfully established
#  !0 - Failed to establish tunnel
# Output variables:
#   TUNNEL_PID - PID of processes used to establish the tunnel.  Used for
#                cleanup at exit.
setup_tunnel_ssh() {
  local R0HOST=$1 PORT=$2

  echo "  Starting ssh tunnel ${R0HOST}:${PORT} -> localhost:${PORT}"
  ssh -N -T -R ${PORT}:localhost:${PORT} ${R0HOST} &
  TUNNEL_PID=$!
  sleep 1
  if ! ps -p ${TUNNEL_PID} > /dev/null
  then
    echo "  Error: Failed to start ssh tunnel"
    return 1
  fi
}

# Use socat to setup a tunnel to localhost on a given port
# Input arguments:
#   IF   - Network interface to listen on
#   DST  - Destination to forward connections to
#   PORT - Port number to establish the reverse tunnel for
#   LOG  - Log file to write to (optional)
# Return code:
#   0 - Tunnel successfully established
#  !0 - Failed to establish tunnel
# Output variables:
#   TUNNEL_PID - PID of processes used to establish the tunnel.  Used for
#                cleanup at exit.
#   IP         - IP address of the interface specified as an input argument
setup_tunnel_socat() {
  local IF=$1 DST=$2 PORT=$3 LOG=$4
  if [ -z "${LOG}" ]
  then
    LOG=socat.log
  fi

  echo -n "  Getting IP for interface ${IF}: "
  IP_OUT=$(/sbin/ip addr show dev ${IF} 2>&1)
  if [ $? -ne 0 ]
  then
    echo "Error: ${IP_OUT}"
    return 1
  fi
  IP=$(echo ${IP_OUT} | sed -n 's|.*inet \([0-9\.]*\).*|\1|p')
  echo ${IP}

  echo "  Starting socat tunnel ${IP}:${PORT} -> ${DST}:${PORT}"
  ${BASE_DIR}/socat -d -d -lf ${LOG} \
    tcp4-listen:${PORT},bind=${IP},close,reuseaddr \
    tcp4-connect:${DST}:${PORT} &
  TUNNEL_PID=$!
  sleep 1
  if ! ps -p ${TUNNEL_PID} > /dev/null
  then
    echo "  Error: Failed to start socat tunnel"
    return 1
  fi
}

################################################################################
# Implementation functions for cancel_job
################################################################################

cancel_job_torque() {
  qdel $1 1>/dev/null 2>/dev/null
}

cancel_job_pbspro() {
  qdel $1 1>/dev/null 2>/dev/null
}

cancel_job_sge() {
  qdel $1 1>/dev/null 2>/dev/null
}

cancel_job_slurm() {
  scancel $1 1>/dev/null 2>/dev/null
}

cancel_job_moab() {
  canceljob $1 1>/dev/null 2>/dev/null
}

################################################################################
# Implementation functions for job_status
################################################################################

job_status_torque() {
  case "$(qstat $1 | tail -1 | awk '{ print $5 }')"
  in
    R)  return 0;;
    Q|W|H) return 1;;
    *)  return 2;;
  esac
}

job_status_pbspro() {
  return 3
}

job_status_sge() {
  case "$(qstat | awk -v JOBID=$1 '$1==JOBID { print $5 }')"
  in
    r|tr)  return 0;;
    qw|tw|t) return 1;;
    *)  return 2;;
  esac
}

job_status_slurm() {
  case "$(squeue -j $1 -o %t | tail -1)"
  in
    R) return 0;;
    PD) return 1;;
    *) return 2;;
  esac
}

job_status_moab() {
  return 3
}

################################################################################
# The following functions must be overriden in a site-specific config file
################################################################################

# Setup a network tunnel to facilitate bouncing the pvserver connection
# prior to job submission
# Input arguments: None
# Return code:
#   0 - Tunnel successfully established
#  !0 - Failed to establish tunnel
# Output variables:
#   TUNNEL_PID     - PID of processes used to establish the tunnel.  Used for
#                    cleanup at exit.
#   PV_SERVER_HOST - Hostname or IP address for pvserver to establish the
#                    reverse connection to.
setup_tunnel_pre() {
  export PV_SERVER_HOST=localhost
  return 0
}

# Setup a network tunnel to facilitate bouncing the pvserver connection
# after the job is allocated
# Input arguments: None
# Return code:
#   0 - Tunnel successfully established
#  !0 - Failed to establish tunnel
# Output variables:
#   TUNNEL_PID     - PID of processes used to establish the tunnel.  Used for
#                    cleanup at exit.
setup_tunnel_post() {
  if [ -n "${TUNNEL_PID}" ]
  then
    return 0
  fi
  return 1
}

# Submit the pvserver job
# Input arguments: None
# Input variables:
#   NUM_NODES       - Number of requested nodes
#   NUM_PPN         - Number of requested processes-per-node
#   PV_SERVER_HOST  - Host for pvserver to connect to
#   PV_SERVER_PORT  - Port number for pvserver to connect to
#   PV_VERSION_FULL - Full ParaView version to use (i.e. 5.1.2, 5.2.0, etc.)
#   [Any other vars passed on the command line]
# Return code:
#   0 - Job successfully submitted
#  !0 - Failed to submit job
# Output variables:
#   JOBID - The ID if the submitted job
submit_job() {
  echo "  Error: submit_job must be overridden"
  return 1
}

# Get the status of a submitted job
# Input arguments:
#   JOBID - ID of the job to check for
# Return code:
#   0 - Job is running
#   1 - Job is in the queue, waiting execution
#   2 - Job is either not present or has finished executing (success or failure)
job_status() {
  echo "  Error: job_status must be overridden"
  return 3
}

# Cancel a running job
# Input arguments:
#   JOBID - ID of the job to end
cancel_job() {
  echo "  Error: cancel_job must be overridden"
  return 1
}
